
#include <Arduino.h>
#include <ArduinoOTA.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include <ESP8266mDNS.h>
// Root topic
extern String propName;
const String deviceTopic = String("paradox/props/") + propName;

// upstairs prop expected to provide these as externs (needs refactor)
extern String   propName;
extern String   propState;
extern String   propMessage;
extern String   propVersion;
extern String   propIPAddress;
extern int      countdownTime;
extern int      secondsLeft;
extern char     displayClockText[5];
extern bool     WIRE_state[5];
extern void     checkBattery();
extern float    battVoltage;
extern float    adcValue;
extern float    lowBattVolt;
extern float    vRefLowADC ;
extern float    vRefLow ;
extern float    vRefHighADC;
extern float    vRefHigh ;
extern int      Adafruit_Brightness;
extern int      holdSecondsLeft;
extern PubSubClient mqtt_client;
extern char*    mqtt_server;
//extern void executeCommand(const String&);

static constexpr size_t JSON_BUFFER_LIMIT = 256;

// *****************************************************************
// ***                handle mqtt callbacks                      ***
// *****************************************************************
void mqttEventCB(char* topic, uint8_t* payload, unsigned int len)
{
  String topicStr(topic);
  auto lspos = topicStr.lastIndexOf('/');
  if (lspos > 0) {
    const auto& p1 = topicStr.substring(0, lspos);
    const auto& p2 = topicStr.substring(lspos + 1, topicStr.length());
    if (p1 == deviceTopic) {
      if (p2 == "command") {
        String payloadStr;
        payloadStr.reserve(len + 1);
        for (auto jj = 0; jj < len; jj++)
          payloadStr.concat((char)payload[jj]);
        Serial.print("Execute: ");
        Serial.println(payloadStr);
       // executeCommand(payloadStr);
      }
    }
  }
}


// *****************************************************************
// ***                Send one message to mqtt broker            ***
// *****************************************************************
void mqtt_send(const String& rootTopic, const String& subTopic, const ArduinoJson::JsonObject& obj)
{
  char data[JSON_BUFFER_LIMIT];
  obj.printTo(data, sizeof(data));

  String topic = rootTopic + '/' + subTopic;
  Serial.print("topic: ");
  Serial.print(topic);
  Serial.print(", data=");
  Serial.println(data);

  // whole thing ignored unless we have a valid server name
  if(!*mqtt_server)
      return;

  while (!(mqtt_client.connected() &&
           mqtt_client.publish(topic.c_str(), data, true))) {
    Serial.print("Attempting MQTT connection to ");
    Serial.print(mqtt_server);
    if (mqtt_client.connect("ESP8266Client")) {
      Serial.println("...Succeeded");
      mqtt_client.setCallback(mqttEventCB);
      topic = deviceTopic + "/command/#";
      Serial.print("subscribing to: ");
      Serial.println(topic);
      mqtt_client.subscribe(topic.c_str(), 0);

      break;
    } else {
      Serial.println("mqtt failed...");
      break; // total hack, until we get the mqtt flags setup right.
    }
  }
}

// *****************************************************************
// ***               Package Prop Status as JSON                 ***
// *****************************************************************
void pushPropStatus()            // Should appear in main sketch.
{
  ArduinoJson::StaticJsonBuffer<JSON_BUFFER_LIMIT> jsonBuffer;
  auto& obj = jsonBuffer.createObject();
  obj["State"] = propState;          // Prop State: [ready, running, paused, solved, failed, expired, starting, waiting, error]
  obj["TLeft"] = secondsLeft;        // Time remainint till prop expires as shown on timer.
  obj["DText"] = displayClockText;   // Time left in "mm:ss".
  obj["HLeft"] = holdSecondsLeft;    // Time left when holding after end of game.

  // todo
  //for (int i; i < 5; i++)                 // Current state of wires 0-4.
  //    {
  //        // state["WIRE_state[" + i + "]"] = WIRE_state[i];
  //    }

  mqtt_send(deviceTopic, "status", obj);
}

void pushPropInfo()
{
  ArduinoJson::StaticJsonBuffer<256> jsonBuffer;
  auto& obj = jsonBuffer.createObject();

  obj["Name"]    = propName;      // Defined in top of sketch as a global.
  obj["IP"]      = propIPAddress; // Send prop IP address for commands and direct access.
  obj["Version"] = propVersion;   // Type and version of prop.
  obj["Message"] = propMessage;   // Message from prop.
  mqtt_send(deviceTopic, "info", obj);
}

void pushPropBattery()
{
  checkBattery();                   // Update battery readings.
  ArduinoJson::StaticJsonBuffer<256> jsonBuffer;
  auto& obj = jsonBuffer.createObject();

  obj["Voltage"] = battVoltage;     // Current voltage reading of battery.
  obj["ADC"] = adcValue;            // Current ADC reading of battery voltage.
  mqtt_send(deviceTopic, "battery", obj);
}

void pushPropEverything()
{
  pushPropStatus();
  pushPropInfo();
  pushPropBattery();
}


