/*
  Company:Paradox, Inc.
  Project: NodeMcu ESP8266 Wifi Bomb Prop
  Engineer: Mark Stevens, Nikhil.P.Lokhande, Clay Hopperdietzel.

  // RFID Prop
  //
  // Designed specifically to work with MyFair 13 GHz tags.
  // ----> Can read up to 8 tags.
  // ----> Readers that go off-line may cause resets.
  // ----> Sends MQTT messages on arrival of new tags.
  // ----> Departure of tags faked by tracking tags to clear out prior position.
  // Not released for public usage without explicit written permission from author.
  // Copyright(c) 2017 by Paradox, Inc.
*/

#include <Arduino.h>
#include <ArduinoOTA.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include <ESP8266mDNS.h>
//#include <WiFiManager.h>
#include <DNSServer.h>        // Included in lib, needed for captive portal
//#include "./DNSServer.h"    // For patched DNSServer file
#include <memory>
#include "FS.h"
#include "Chrono.h"
// #include <RBD_Button.h>
#include <Wire.h>
#include <String>
#include "MFRC522_I2C.h"
// #include <ParadoxProp.h>

// *** Set up MQTT Responses ***
#include "mqttStuff.h"

// *** Define GPIO Pins ***
#include "nodemcuv2.h"

// *** WiFi Configuration ***

String Chipid = String(ESP.getChipId());

// *** Prop Information ***
String propVersion = "RFID_V1.1";              // Type and version. UPDATE EACH RELEASE!!!

// *** fixme - fake so web page works for all props for now
// String propName = String("RFIDProp_") + Chipid.substring(Chipid.length() - 4, Chipid.length());
String propName = String("RFIDProp_") + "E007";

// *** WiFi Configuration ***
WiFiClient espClient;
PubSubClient mqtt_client(espClient);
const int webserver_port = 80;
ESP8266WebServer web_server(webserver_port);

// *** WiFi Connection Parameters ***
DNSServer dnsServer;
const byte        DNS_PORT = 53;
char* mqtt_server = "";  // Migh want to change the default for this.
char* ssid;
char* pass;
String Sssid, Spass, Smqtt_s;
IPAddress         SIP(192, 168, 4, 1);
String propIPAddress = WiFi.localIP().toString();
bool Login = false;
String Udline, Mdline;
bool Filedata = false;
bool Config = false;
// String Chipid = String(ESP.getChipId());
char* cChipid;                              // CHIP is the last for digits of the Chipid?

// *** Set Host Name ***
String Shost = ("ParadoxProp_" + Chipid.substring((Chipid.length()) - 4, (Chipid.length())));
const char* host = "ParadoxBprop";
//strcpy(host,Shost.c_str());

// *** Prop State & Message ***
String propState = "starting";      // Prop State: [ready, running, paused, solved, failed, expired, starting, waiting, error]
String propMessage = "";            // Message from prop.

// *** Battery Monitor Settings ***
float vRefLowADC = 95;             // ADC Reading of Integer Point x(A) to Calibrate
float vRefLow = 0.28;              // Volatage Reading of Float Point y(A) to Calibrate
float vRefHighADC = 218;           // ADC Reading of Integer Point x(B) to Calibrate
float vRefHigh = 0.67;             // Volatage Reading of Float Point y(B) to Calibrate
float vM;                          // Slope of conversion (calculated)
float vB;                          // Y intercept of conversion
float adcValue;                    // Current ADC Reading  (last polled)
float battVoltage;                 // Current battery voltage (last polled)
float lowBattVolt;                 // Voltage at which battery needs replacement.


// *** Setup RFID Stuff ***
// using namespace Paradox;
#define NUM_READERS 8
#define TAG_SIZ 1024
String RFID_JSON = "";
int startLoc;
int stopLoc;
MFRC522 mfrc522(0x28, RST_PIN);
MFRC522::MIFARE_Key key;
#define TCAADDR 0x70
Chrono tcatimer;
Chrono errTimers[NUM_READERS];
Chrono heartbeat;
bool isErr[NUM_READERS];
String data[NUM_READERS];

// ??? What does this do? ???
void tcaselect(uint8_t i) {
  if (i > 7) return;

  Wire.beginTransmission(TCAADDR);
  Wire.write(1 << i);
  Wire.endTransmission();
  tcatimer.restart(0);
  while (!tcatimer.hasPassed(50)) //50ms delay to let it register
    continue;
  tcatimer.stop();
  return;
}

void RFID_Dump(MFRC522::Uid *uid, MFRC522::MIFARE_Key *key) {
  int x = 0;
  char current;
  bool inJson = false;
  int numBrack = 0;
  for (int i = 15; i >= 0; i--) {
    x++;
    byte status;
    byte firstBlock;
    byte no_of_blocks;
    bool isSectorTrailer;
    byte c1, c2, c3;
    byte c1_, c2_, c3_;
    bool invertedError;
    byte g[4];
    byte group;
    bool firstInGroup;
    no_of_blocks = 4;
    firstBlock = i * no_of_blocks;
    byte byteCount;
    byte buffer[18];
    byte blockAddr;
    isSectorTrailer = true;
    for (int8_t blockOffset = no_of_blocks - 1; blockOffset >= 0; blockOffset--) {
      blockAddr = firstBlock + blockOffset;
      if (isSectorTrailer) {
        status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, firstBlock, key, uid);
        if (status != MFRC522::STATUS_OK) {
          Serial.print(F("PCD_Authenticate() failed: "));
          Serial.println(mfrc522.GetStatusCodeName(status));
          return;
        }
        isSectorTrailer = false;
      }
      byteCount = sizeof(buffer);
      status = mfrc522.MIFARE_Read(blockAddr, buffer, &byteCount);
      if (status != MFRC522::STATUS_OK) {
        Serial.print(F("MIFARE_Read() failed: "));
        Serial.println(mfrc522.GetStatusCodeName(status));
        continue;
      }
      for (byte index = 0; index < 16; index++) {
        current = (char)buffer[index];
        //Serial.print(current);

        // Serial.print("buffer: ");
        // Serial.println(buffer[3]);
        //Serial.print(current);
        if (inJson) {
          RFID_JSON += current;
          if (current == '{') {
            numBrack++;
          }
          else if (current == '}') {
            numBrack--;
            if (numBrack <= 0) {
              return;
            }
          }
        }
        else if (current == '{') {
          inJson = true;
          numBrack++;
          RFID_JSON += current;
        }
      }

    }
  }
  RFID_JSON = ""; //If we get here, then anything we read previously is garbage
}

//class ParadoxRFIDProp : public Prop {
//  public:
void publishHeartbeat() {

  ArduinoJson::DynamicJsonBuffer jbuf;
  auto& obj = jbuf.createObject();
  obj["readerNum"] = 8;
  obj["Status"] = "ok";
  obj["Msg"] = "heartbeat";
  Serial.println("heartbeat");
//  publishEvent(obj);                      // !!!!!! Need to publish!!!!!!
}

void publishStatus(int i, bool x) {
  ArduinoJson::DynamicJsonBuffer jbuf;
  auto& obj = jbuf.createObject();
  obj["readerNum"] = i;
  obj["Status"] = "ok";
  const char *jsonPublish = RFID_JSON.c_str();
  obj["RFIDdata"] = jsonPublish;
  Serial.print("Reader: ");
  Serial.print(i);
  Serial.print(" Data: ");
  if (x) {
    obj["state"] = "on";
  }
  else {
    obj["state"] = "off";
  }
  Serial.println(jsonPublish);
//  publishEvent(obj);                      // !!!!!! Need to publish!!!!!!
}

void publishError(int i, bool isOk) {
  ArduinoJson::DynamicJsonBuffer jbuf;
  auto& obj = jbuf.createObject();
  obj["readerNum"] = i;
  if (!isOk)
  {
    obj["Msg"] = "ERROR! READER NOT RESPONDING";
    obj["Status"] = "fail";
  }
  else
  {
    obj["Msg"] = "Back online";
    obj["Status"] = "ok";
  }
//  publishEvent(obj);                      // !!!!!! Need to publish!!!!!!
}


//     ParadoxRFIDProp(): Prop("ParadoxRFIDProp", "1.0", 50) {}

//virtual bool setup_readers() {
//  Prop::setup();
void setup_readers() { 
  for (int i = 0; i < NUM_READERS; i++) {
    isErr[i] = false;
    data[i] = "";
  }
  Wire.begin(SDA_PIN, SCL_PIN);
  for (uint8_t i = 0; i < NUM_READERS; i++) {
    tcaselect(i);
    byte v = mfrc522.PCD_ReadRegister(mfrc522.VersionReg); //Make sure it's there
    if ((v == 0x00) || (v == 0xFF)) {//Something's wrong
      Serial.print("Error on reader: ");
      Serial.println(i);
    }
    else
      mfrc522.PCD_Init();   // Init MFRC522
  }
  heartbeat.restart(0);
//  addTimer(Timer(Timer::Repeating, 50,                        // !!!!! Need timer!!!!!
//  [this](const Paradox::Timer & t) {
//    looper();
//  }));
  //  Serial.println("end setup");
}


void looper() {
  if (heartbeat.elapsed() >= 10000) {
    publishHeartbeat();
    heartbeat.restart(0);
  }
  uint8_t select = 0;
  for (int i = 0; i < NUM_READERS; i++) {
    //Serial.println(i);
    RFID_JSON = "";
    // Serial.println("loop");
    tcaselect(select);
    select += 1;
    // Look for new cards
    byte v = mfrc522.PCD_ReadRegister(mfrc522.VersionReg); //Make sure it's there
    if ((v == 0x00) || (v == 0xFF)) {//Something's wrong
      if (isErr[i]) {
        if (errTimers[i].hasPassed(10000)) { //dont want to flood serial and mqtt with error
          Serial.print("Error on reader: ");
          Serial.println(i);
          publishError(i, false);
          errTimers[i].restart(0);
        }
      }
      else {
        errTimers[i].restart(0);

        Serial.print("Error on reader: ");
        Serial.println(i);
        publishError(i, false);
        isErr[i] = true;
      }
    }
    else if (isErr[i]) {
      Serial.print("Reader: ");
      Serial.print(i);
      Serial.print(" back online!");
      mfrc522.PCD_Init();
      //publishError(i, true);
      isErr[i] = false;
      errTimers[i].stop();
    }


    if (!isErr[i]) {
      if ( ! mfrc522.PICC_IsNewCardPresent()) {
        continue;
      }

      // Select one of the cards
      if ( ! mfrc522.PICC_ReadCardSerial()) {
        continue;
      }

      //Serial.println("found card");
      // Dump into an array
      MFRC522::MIFARE_Key key;
      for (byte x = 0; x < 6; x++) {
        key.keyByte[x] = 0xFF;
      }
      RFID_Dump(&(mfrc522.uid), &key);
      //mfrc522.PICC_DumpToSerial(&(mfrc522.uid));
      mfrc522.PICC_HaltA(); //Halt it
      mfrc522.PCD_StopCrypto1();
      if (RFID_JSON != "") {
        for (int t = 0; t < NUM_READERS; t++) {
          if (RFID_JSON == data[t]) {
            publishStatus(t, false);
            data[t] = "";
          }
        }
        data[i] = RFID_JSON;
        publishStatus(i, true);
      }
    }
  }
}
void onIdle(unsigned long freeMillis) {}



// *****************************************************************
// ***                 Blink LED to Show Activity                ***
// *****************************************************************
class ActivityBlink
{
  public:
    ActivityBlink() {
      digitalWrite(LED_PIN, 0);
    }
    ~ActivityBlink() {
      digitalWrite(LED_PIN, 1);
    }
};


// *****************************************************************
// ***                       Check Battery                       ***
// *****************************************************************
void checkBattery() {

  // Calibration of the ADC to read the volatage of the supply battery.
  vM = (vRefHigh - vRefLow) / (vRefHighADC - vRefLowADC);
  // Serial.print("Voltage conversion: vM = ");
  // Serial.print(vM);
  vB = vRefLow - (vM * vRefLowADC);
  // Serial.print(" and vB = ");
  //Serial.println(vB);

  // Read and calculate voltage
  adcValue = analogRead(A0);                // Get integer volatage value from ADC
  battVoltage = (vM * adcValue) + vB;       // Calculate supply voltage
}


// *****************************************************************
// ***                  Server Page Definition                   ***
// *****************************************************************

//Server Page
const char HTTP_HEAD[] PROGMEM            = "<!DOCTYPE html><html lang=\"en\"><head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1, user-scalable=no\"/><title>{v}</title>";
const char HTTP_STYLE[] PROGMEM           = "<style>.c{text-align: center;} div,input{padding:5px;font-size:1em;} input{width:95%;} body{text-align: center;font-family:verdana;} button{border:0;border-radius:0.3rem;background-color:#1fa3ec;color:#fff;line-height:2.4rem;font-size:1.2rem;width:100%;} .q{float: right;width: 64px;text-align: right;} .l{background: url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAAALVBMVEX///8EBwfBwsLw8PAzNjaCg4NTVVUjJiZDRUUUFxdiZGSho6OSk5Pg4eFydHTCjaf3AAAAZElEQVQ4je2NSw7AIAhEBamKn97/uMXEGBvozkWb9C2Zx4xzWykBhFAeYp9gkLyZE0zIMno9n4g19hmdY39scwqVkOXaxph0ZCXQcqxSpgQpONa59wkRDOL93eAXvimwlbPbwwVAegLS1HGfZAAAAABJRU5ErkJggg==\") no-repeat left center;background-size: 1em;} h1{color:blue;} h3{color:blue;} p{color:blue;}</style>";
const char HTTP_SCRIPT[] PROGMEM          = "<script>function c(l){document.getElementById('s').value=l.innerText||l.textContent;document.getElementById('p').focus();}</script>";
const char HTTP_HEAD_END[] PROGMEM        = "</head><body><div style='text-align:left;display:inline-block;min-width:260px;'>";
const char HTTP_PORTAL_OPTIONS[] PROGMEM  = "<form action=\"/wifi\" method=\"get\"><button>Configure WiFi</button></form><br/><form action=\"/Files\" method=\"get\"><button>Files on Server</button></form><br/><form action=\"/i\" method=\"get\"><button>Info</button></form><br/><form action=\"/r\" method=\"post\"><button>Reset</button></form>";
const char HTTP_PORTAL_OPTIONS1[] PROGMEM  = "<form action=\"/start\" method=\"get\"><button>Start</button></form><br/><form action=\"/pause\" method=\"get\"><button>Pause</button></form><br/><form action=\"/reset\" method=\"get\"><button>Reset</button></form><br/><form action=\"/status\" method=\"post\"><button>Status</button></form></br><form action=\"/clearUD\" method=\"post\"><button>Clear User Data</button></form><br/><form action=\"/mqttconfig\" method=\"get\"><button>MQTT Config</button></form>";

const char HTTP_ITEM[] PROGMEM            = "<div><a href='#p' onclick='c(this)'>{v}</a>&nbsp;<span class='q {i}'>{r}%</span></div>";
const char HTTP_FORM_START[] PROGMEM      = "<form method='get' action='wifisave'><input id='s' name='s' length=32 placeholder='SSID'><br/><input id='p' name='p' length=64 type='password' placeholder='password'><br/>";
const char HTTP_FORM_PARAM[] PROGMEM      = "<br/><input id='{i}' name='{n}' length={l} placeholder='{p}' value='{v}' {c}>";
const char HTTP_FORM_END[] PROGMEM        = "<br/><button type='submit'>save</button></form>";
const char HTTP_SCAN_LINK[] PROGMEM       = "<br/><div class=\"c\"><a href=\"/wifi\">Scan</a></div>";
const char HTTP_SAVED[] PROGMEM           = "<div>Credentials Saved<br />Trying to connect ESP to network.<br />If it fails reconnect to AP to try again</div>";
const char HTTP_END[] PROGMEM = "</div></body></html>";
File logo;

// *****************************************************************
// ***                       Start as AP                      ***
// *****************************************************************
void Apmode()
{
  // *** Setup and Start Server ***
  setupHTTPRoutes();
  web_server.begin();

  if (Login == false || Filedata == false )
  {
    // *** Reconnect or Configure Access Point ***
    String temp;
    temp = "Paradox_" + Chipid.substring((Chipid.length()) - 3, (Chipid.length()));
    cChipid = new char [Chipid.length() + 1];
    strcpy(cChipid, temp.c_str());
    Serial.println(cChipid);
    WiFi.mode(WIFI_AP);
    // *** DNS captive portal redirect ***
    dnsServer.start(DNS_PORT, "*", SIP);
    WiFi.softAP(cChipid, "MCEscher");
    WiFi.softAPConfig(SIP, SIP, IPAddress(255, 255, 255, 0));
    Serial.print("HTTP Server started: http://");
    Serial.print(WiFi.softAPIP());
    Serial.print(':');
    Serial.println(webserver_port);
  }
}


// *****************************************************************
// ***                      Connect to WiFI                      ***
// *****************************************************************
void setup_wifi()
{
  // *** Setup and Start Server ***
  setupHTTPRoutes();
  web_server.begin();

  Serial.println("Wifi-Setup");
  Serial.print("Filedata:");
  Serial.println(Filedata);
  if (Filedata == true or Config == true)
  {
    Serial.println("Begin to connect");
    // connect to wifi
    WiFi.mode(WIFI_STA);
    Serial.println();
    Serial.print("Connecting");
    WiFi.begin(ssid, pass);
    int c = 0;
    while (WiFi.status() != WL_CONNECTED)
    {
      delay(500);               // Keep trying for 30 seconds.
      Serial.print(".");
      c += 1;
      if (c == 60)
      {
        Serial.println("Unable to connect");
        Apmode();
        break;
      }
    }
    if (WiFi.status() == WL_CONNECTED)
    {
      Login = true;
      Filedata = true;
      Config = false;
      Serial.println("");
      Serial.println("WiFi connected");
      Serial.println("IP address: ");
      Serial.println(WiFi.localIP());
      MDNS.begin(host);

      propIPAddress = WiFi.localIP().toString();

      // *** Setup MQTT Communication ***
      Serial.print("Trying MQTT connection to ");
      Serial.println(mqtt_server);
      mqtt_client.setServer(mqtt_server, 1883);

    }
  }
}


// *****************************************************************
// ***                 Server Response Functions                 ***
// *****************************************************************
void handleFileList() {

  String path = web_server.arg("dir");
  Serial.println("handleFileList: " + path);
  Dir dir = SPIFFS.openDir(path);
  path = String();

  String output = "Current SPIFFS Files:[ ";
  while (dir.next()) {
    File entry = dir.openFile("r");
    if (output != "[") output += ',';
    bool isDir = false;
    output += "{\"type\":\"";
    output += (isDir) ? "dir" : "file";
    output += "\",\"name\":\"";
    output += String(entry.name()).substring(1);
    output += "\"}";
    entry.close();
  }
  output += "]";
  web_server.send(200, "text/plain", output);
}

// *****************************************************************
// ***                   Configure HTTP Routes                   ***
// *****************************************************************
void setupHTTPRoutes() {

  // *** Set some routes for the http server ***

  // *** Server Homepage ***
  web_server.on("/", []() {
    ActivityBlink ab;
    if (!Login)
    {
      String page = FPSTR(HTTP_HEAD);
      page.replace("{v}", "Configuration WiFi Page");
      page += FPSTR(HTTP_SCRIPT);
      page += FPSTR(HTTP_STYLE);
      page += "2017 Paradox Game Rooms";
      page += FPSTR(HTTP_HEAD_END);
      page += "<h1>";
      page += "Paradox Bomb Prop WiFi Configuration";
      page += "</h1>";
      page += F("<h3>WiFiManager</h3>");
      page += FPSTR(HTTP_PORTAL_OPTIONS);
      page += "<p> After connecting prop to your router, visit http://ParadoxBprop.local/ for controls page </p>";
      page += FPSTR(HTTP_END);
      // web_server.streamFile(logo,"image/png");
      web_server.send(200, "text/html", page.c_str());
    }
    else
    {
      String page = FPSTR(HTTP_HEAD);
      page.replace("{v}", "Paradox B Prop");
      page += FPSTR(HTTP_SCRIPT);
      page += FPSTR("<style>.c{text-align: center;} div,input{padding:5px;font-size:1em;} input{width:95%;} body{ background-image: url(\"http://res.cloudinary.com/lynuua8ep/image/upload/v1488136906/ParadoxProdWeb_e29gcp.png\");background-repeat: no-repeat; text-align: center;font-family:verdana;} button{border:0;border-radius:0.3rem;background-color:#1fa3ec;color:#fff;line-height:2.4rem;font-size:1.2rem;width:100%;} .q{float: right;width: 64px;text-align: right;} .l{background: url(\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAAALVBMVEX///8EBwfBwsLw8PAzNjaCg4NTVVUjJiZDRUUUFxdiZGSho6OSk5Pg4eFydHTCjaf3AAAAZElEQVQ4je2NSw7AIAhEBamKn97/uMXEGBvozkWb9C2Zx4xzWykBhFAeYp9gkLyZE0zIMno9n4g19hmdY39scwqVkOXaxph0ZCXQcqxSpgQpONa59wkRDOL93eAXvimwlbPbwwVAegLS1HGfZAAAAABJRU5ErkJggg==\") no-repeat left center;background-size: 1em;} h1{color:blue;} h3{color:blue;} p{color:blue;}</style>");

      page += "2017 Paradox Game Rooms";
      page += FPSTR(HTTP_HEAD_END);
      page += "<br><br>";
      page += "<h1>";
      page += "Paradox Bomb Prop Controls";
      page += "</h1>";
      page += F("<h3>Bomb Prop Controls</h3>");
      page += FPSTR(HTTP_PORTAL_OPTIONS1);
      page += "<p> Instructions: The Paradox Bomb Prop has connected to the router now";
      page += " please use this IP in browser as address for connection on this network:";
      page += ssid;
      page += ",IP :";
      page += (WiFi.localIP().toString());
      page += " or ParadoxBprop.local. You may disconnect from the Bomb Prop AP</p>";
      page += FPSTR(HTTP_END);

      web_server.send(200, "text/html", page.c_str());
    }
  });


  // *** Config page ***
  web_server.on("/wifi", HTTP_GET, []() {
    ActivityBlink ab;
    String page = FPSTR(HTTP_HEAD);
    page.replace("{v}", "Config ESP");
    page += FPSTR (HTTP_SCRIPT);
    page += FPSTR(HTTP_STYLE);
    page += "Config";
    page += FPSTR(HTTP_HEAD_END);
    int N = WiFi.scanNetworks();

    if (N == 0)
    {
      page += F("No networks found. Refresh to scan again.");
    }
    else
    {
      int indices[N];
      for (int i = 0; i < N; i++)
      {
        indices[i] = i;
        page += ("<p>" + WiFi.SSID(i) + "</p> \n");
      }
    }

    page += FPSTR(HTTP_FORM_START);
    page += FPSTR(HTTP_FORM_END);
    page += FPSTR(HTTP_SCAN_LINK);
    page += FPSTR(HTTP_END);

    web_server.send(200, "text/html", page.c_str());
  });


  // *** Config Save page ***
  web_server.on("/wifisave", []() {
    int cnt = 0;
    char* c = ",";
    String msg;
    Config = true;
    Sssid = web_server.arg("s");
    Spass = web_server.arg("p");
    ssid = new char[Sssid.length() + 1];
    strcpy(ssid, Sssid.c_str());

    pass = new char[Spass.length() + 1];
    strcpy(pass, Spass.c_str());
    Serial.print(ssid);
    Serial.println(pass);
    Serial.println();
    Serial.print("Connecting");
    msg += "Attempting connection, when connected the AP will stop automatically.";
    web_server.send(200, "text/plain", msg);
    setup_wifi();
    if (Login)
    {
      msg += "Data saved. Please go to homepage and refresh for instructions";
      File Userdata = SPIFFS.open("/router_auth.txt", "r+");
      for (int p = 0; p < Sssid.length(); ++p)
      {
        Userdata.write((ssid[p]));
      }
      Userdata.write(*c);
      for (int p = 0; p < Spass.length(); ++p)
      {
        Userdata.write((pass[p]));
      }
    }
    else
    {
      msg += "Unable to connect try again.";
    }
    web_server.send(200, "text/plain", msg);
  });

  //File List Test
  web_server.on("/Files", HTTP_GET, handleFileList);

  // *** Clear file system page ***
  web_server.on("/clearUD", []() {

    SPIFFS.remove("/router_auth.txt");

    String msg;
    if (!SPIFFS.exists("/router_auth.txt"))
    {
      msg += "User data file removed";
    }
    else
    {
      msg += "Error deleting user data file";
    }

    web_server.send(200, "text/plain", msg);
  });


  // *** MQTT system config page ***
  web_server.on("/mqttconfig", HTTP_GET, []() {   //
    ActivityBlink ab;
    String page = FPSTR(HTTP_HEAD);
    page.replace("{v}", "MQTT Config");
    page += FPSTR (HTTP_SCRIPT);
    page += FPSTR(HTTP_STYLE);
    page += "MQTT Config";
    page += "<p> Current MQTT Server:";
    page += mqtt_server;
    page += ". </p>";
    page += FPSTR(HTTP_HEAD_END);
    page += "<form method='get' action='mqttsave'><input id='m' name='m' length=32 placeholder='MQTT Server'><br/>";
    page += FPSTR(HTTP_FORM_END);
    page += "<p> Save the MQTT server to connect here. </p>";
    page += FPSTR(HTTP_END);

    web_server.send(200, "text/html", page.c_str());
  });

  // *** MQTT system config save page ***
  web_server.on("/mqttsave", []() {
    String msg;
    Smqtt_s = web_server.arg("m");
    mqtt_server = new char[Smqtt_s.length() + 1];
    strcpy(mqtt_server, Smqtt_s.c_str());
    mqtt_client.setServer(mqtt_server, 1883);
    mqtt_client.connect("ESP8266Client");
    Serial.println(mqtt_client.state());

    if (mqtt_client.connected())
    {
      msg += "MQTT data saved";
      File Mqttdata = SPIFFS.open("/mqtt_auth.txt", "r+");
      for (int p = 0; p < Smqtt_s.length(); ++p)
      {
        Mqttdata.write((mqtt_server[p]));
      }
    }
    else
    {
      msg += "Unable to connect data not saved";
    }

    web_server.send(200, "text/plain", msg);

  });


}

// *****************************************************************
// ***                   File system setup                       ***
// *****************************************************************
void setupFileSystem()
{
  FSInfo fsi;
  if (SPIFFS.info(fsi))
  {
    Serial.print("Total Bytes: ");
    Serial.println(fsi.totalBytes);
    Serial.print("Used Bytes: ");
    Serial.println(fsi.usedBytes);
    Serial.print("Max Path Length: ");
    Serial.println(fsi.maxPathLength);
    Serial.print("FS exists , path is:");
    Serial.println(SPIFFS.exists("/router_auth.txt"));
    logo = SPIFFS.open("/ParadoxProdWeb.png", "r");
  }

  // *** Initialize userdata file cache ***
  Serial.println("Caching");
  File Userdata;
  File Mqttdata;

  Userdata = SPIFFS.open("/router_auth.txt", "r");
  Mqttdata = SPIFFS.open("/mqtt_auth.txt", "r");
  if (!Userdata)
  {
    Serial.println("Error Opening File");
    Serial.println("Creating file");
    File Userdata = SPIFFS.open("/router_auth.txt", "w+");
  }
  if (!Mqttdata)
  {
    Serial.println("Error Opening File");
    Serial.println("Creating file");
    File Mqttdata = SPIFFS.open("/mqtt_auth.txt", "w+");
  }

  Udline = Userdata.readString();
  Mdline = Mqttdata.readString();


  if (Udline.length() == 0 )
  {
    Serial.println("Userdata File empty");
  }

  else
  {
    Filedata = true;
    //Serial.println("Connecting using:");
    //Serial.println(Udline);
    int split = Udline.indexOf(",");
    Sssid = Udline.substring(0, split);
    Spass = Udline.substring(split + 1);

    ssid = new char[Sssid.length() + 1];
    strcpy(ssid, Sssid.c_str());

    pass = new char[Spass.length() + 1];
    strcpy(pass, Spass.c_str());
    Serial.println("Caching successful");
  }

  if (Mdline.length() == 0 )
  {
    Serial.println("Mqttdata File empty");
  }

  else
  {
    mqtt_server = new char[Mdline.length() + 1];
    strcpy(mqtt_server, Mdline.c_str());
    mqtt_client.setServer(mqtt_server, 1883);
    mqtt_client.connect("ESP8266Client");
    Serial.println(mqtt_client.state());
  }
}


// *****************************************************************
// ***                       Setup Function                      ***
// *****************************************************************
void setup() {

  // *** Initialize file system. ***
  SPIFFS.begin();

  // *** Set up file system ***
  setupFileSystem();

  // *** Initialize digital pin LED_PIN as an output. ***
  pinMode(LED_PIN, OUTPUT);

  // *** Set Serial Output Speed ***
  // Serial.begin(9600);
  Serial.begin(115200);
  Serial.println(' ');

  // *** Setup the RFID Readers ***
  setup_readers();

  // *** Initialize wifi setup, if user data tries to connect else starts configuration AP mode ***
  Serial.println("Trying the very basic!");

  setup_wifi();

  // *** Prepare Input/Output Pins ***
  pinMode(LED_PIN, OUTPUT);      //Onboard LED
  digitalWrite(LED_PIN, LOW);    //Initial state is ON

  if (!Login) {
    // *** DNS captive portal redirect ***
    dnsServer.start(DNS_PORT, "*", SIP);
    Apmode();
  }

  // annouce we are alive
  {
    ArduinoJson::StaticJsonBuffer<128> jsonBuffer;
    auto& obj = jsonBuffer.createObject();
    obj["ip"] = propIPAddress;
    obj["name"] = propName;
    obj["version"] = propVersion;
    mqtt_send("paradox/devices", propName, obj);
  }

  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
  // ArduinoOTA.setHostname("myesp8266");

  // No authentication by default
  // ArduinoOTA.setPassword((const char *)"123");

  ArduinoOTA.onStart([]() {
    Serial.println("Start");
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}


// *****************************************************************
// ***                        Main Loop                          ***
// *****************************************************************
void loop() {

  // Do stuff here.
  looper();

  // delay(loopDelay);
  dnsServer.processNextRequest();
  web_server.handleClient();
  mqtt_client.loop();

  ArduinoOTA.handle();
}

