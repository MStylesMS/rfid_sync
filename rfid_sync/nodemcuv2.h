// *************************************************************************
// ***                           ESP8266-12F (DevKit)                    ***
// *************************************************************************
// *** Pin Assignments for RIGHT Side of board.                          ***
// *** (Internal pullup resistors, so wire switches from pin to ground.) ***
// *************************************************************************
// NAME               GPIO        PIN   Description
// ________________   ____   //   ___   _________________________________________
//                    ADCO   //   A0    Battery voltage monitor
// GND                  .    //  GND    Ground
// VUSB                 .    //   VU    N/A or 5v Power Out
//                     10;   //  SD3    N/A
//                      9;   //  SD2    N/A
//                      .    //  SD1    N/A
//                      .    //  CMD    N/A
//                      .    //  SD0    N/A
//                      .    //  CLK    N/A
// GND                  .    //  GND    Ground
// 3.3Vv                .    //  3V3    3.3v Power Out
// EN                   .    //   EN    N/A
// Reset                .    //  RST    Reset (Keep High, Pull Low to Reset)
// GND                  .    //  GND    Ground
// 5v In                .    //  Vin    5v Power In

// *************************************************************************
// *** Pin Assignments for LEFT Side of board.                           ***
// *** (Internal pullup resistors, so wire switches from pin to ground.) ***
// *************************************************************************
// NAME                 GPIO        PIN   Description
// __________________   ____   //   ___   _________________________________________
const short int LED_PIN = 16;  //   D0    Activity LED
//                         5;  //   D1    
const short int RST_PIN =  4;  //   D2    I2C-RFID Reset
const short int SDA_PIN =  0;  //   D3    SDA for I2C to LED Display
const short int SCL_PIN =  2;  //   D4    SCL for I2C to LED Display
// 3.3Vv                   .   //  3V3    3.3v Power Out
// GND                     .   //  GND    Ground
//                         14; //   D5    
//                         12; //   D6    
//                         13; //   D7    
//                         15  //   D8
//                          3  //   D9
//                          1  //   D10
// GND                      .  //   GND   Ground
// 3.3Vv                    .  //   3V3   3.3v Power Out

// *** Wire disconnection logic dependencies ***
// It crashes on pinMode() if GPIO1 is used.
// It won't startup with GPIO's 0, 2, 15 but runs fine after.
// It will startup with GPIO's 9 and 10 set to INPUT_PULLUP but locks when wires are applied.
// The following setup worked 0, 4, 5, 12, 14, but I needed 12 & 14 for IC2
// The following setup worked but I ran out of GPIO's for the buzzer: 4, 5, 12, 13.

